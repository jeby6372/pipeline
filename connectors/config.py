#!/usr/bin/env python38
# -*- coding: utf-8 -*-
import json
import os

from connectors.http import HttpGet
from errors.db_errors import DatabaseConfigurationError

'''
Base remote configuration class.

Uses environment variables to get configuration server credentials

CONFIGURATION_HOST : host address
CONFIGURATION_PORT : host port
CONFIGURATION_USER : user
CONFIGURATION_PASSWORD : password

'''


class RemoteConfig:
    """
    Fetch the content of a remote json file hosted on the configuration server

    "context" is the url base used by the http server to get data. See below the rewriting rule
    eg : /var/configuration/gulplug/dev/conf.json
    then the context is /gulplug/conf on port 8090
    """

    """
    on port 8090 : rewrite ^(/.*)/(.*) /$1/dev/$2.json break;
    on port 8091 : rewrite ^(/.*)/(.*) /$1/stg/$2.json break;
    on port 8092 : rewrite ^(/.*)/(.*) /$1/prd/$2.json break;
    """

    __context = None
    __host = None
    __port = None
    __user = None
    __password = None

    def __init__(self, context, logger=None):
        self.__context = context
        self.__host = os.environ['CONFIGURATION_HOST']
        self.__port = os.environ['CONFIGURATION_PORT']
        self.__user = os.environ['CONFIGURATION_USER']
        self.__password = os.environ['CONFIGURATION_PASSWORD']

    def pull(self):
        url = 'http://' + self.__host + ':' + self.__port + '/' + self.__context
        request = HttpGet(url, self.__user, self.__password)

        return request.run()


'''
Parses a json structure to provide a connection template object
{
    context:service_name,
    host:server_address,
    port:server_port,
    user:user_name,
    password:password_value,
    database:database_name
}
'''


class DbConfig:
    source = None

    # class properties
    host = None
    port = None
    database = None
    user = None
    password = None

    def __init__(self, json_data, logger=None):
        self.source = json.loads(json_data)

    def parse(self):
        self.host = self.source["host"]
        self.port = self.source["port"]
        self.database = self.source["database"]
        self.user = self.source["user"]
        self.password = self.source["password"]

        if self.host is None \
                or self.port is None \
                or self.database is None \
                or self.user is None \
                or self.password is None:
            raise DatabaseConfigurationError("Null parameter found in database configuration")

        return self


'''
Parses a json structure to provide a connection template object

{
        "scanner":{
                "path":"/home/WD008EE8/DATA",
                "extension":".csv.gz",
                "subdirs":"true",
                "logpath":"/tmp/notifier.log",
                "loglevel":"DEBUG"
        },

        "loader":{
                "host":"172.27.111.106",
                "notification-uri":"api/loaders/csvfilenotifications",
                "port":4010
        }

}

'''


class NotifierConfig:
    source = None

    # class properties
    path = None
    extension = None
    subdirs = None
    logpath = None
    loglevel = None

    loader_host = None
    loader_port = None
    loader_notification_uri = None

    statements = []

    messages = []

    def __init__(self, json_data):
        self.source = json.loads(json_data)

    def parse(self):
        scanner = self.source["scanner"]
        self.path = scanner["path"]
        self.extension = scanner["extension"]
        self.subdirs = scanner["subdirs"]
        self.logpath = scanner["logpath"]
        self.loglevel = scanner["loglevel"]

        loader = self.source["loader"]
        self.loader_host = loader["host"]
        self.loader_port = loader["port"]
        self.loader_notification_uri = loader["notification-uri"]

        self.statements = self.source["statements"]
        self.messages = self.source["messages"]

        return self
