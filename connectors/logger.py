#!/usr/bin/env python38
# -*- coding: utf-8 -*-

import logging


class Log:

    logger=None

    def __init__(self, log_path, level=logging.DEBUG):
        logger = logging.getLogger(__name__)
        logging.basicConfig(filename=log_path, level=level,
                            format='%(levelname)s:%(asctime)s - %(name)s - %(message)s', datefmt='%Y/%m/%d %H:%M:%S')

        self.logger = logger