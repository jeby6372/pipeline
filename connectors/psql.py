#!/usr/bin/env python38
# -*- coding: utf-8 -*-
import logging

import psycopg2
from errors.db_errors import DatabaseConnectionError


class PgsqlConnector:
    __host = None;
    __port = None
    __database = None;
    __user = None
    __password = None
    __logger = None
    __connection = None

    # config parameter is a model.config.DbConfig object

    def __init__(self, config, logger=None):
        self.__host = config.host
        self.__port = config.port
        self.__database = config.database
        self.__user = config.user
        self.__password = config.password
        self.__logger = logger


    def cursor(self):
        self.__connection = psycopg2.connect(user=self.__user,
                                           password=self.__password,
                                           host=self.__host,
                                           port=self.__port,
                                           database=self.__database)
        return self.__connection.cursor()

    def commit(self):
        self.__connection.commit()


    def close(self):
        self.__connection.close()

        # Note the app responsability to close the cursor

