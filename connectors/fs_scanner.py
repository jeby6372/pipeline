#!/usr/bin/env python38
# -*- coding: utf-8 -*-
import asyncio
import logging
import os
import aionotify
from connectors.http import HttpPut
from dao.dao_notifications import FileNotificationDao
from model.notifications import FileNotification as Notification


class Scanner:
    loop = None
    watcher = None
    path = None
    dao = None
    config = None

    __logger = None

    # app_config is a NotifierConfig object
    def __init__(self, app_config, logger):

        # listener initialization
        watcher = aionotify.Watcher()
        watcher.watch(alias=app_config.path, path=app_config.path, flags=aionotify.Flags.CLOSE_WRITE)

        # class properties setup
        self.watcher = watcher
        self.config = app_config
        self.__logger = logger

    async def work(self, db_connector):
        await self.watcher.setup(self.loop)
        dao = FileNotificationDao(db_connector, self.__logger)

        # start loop on events
        while True:
            file = await self.watcher.get_event()
            self.__logger.debug("new file detected :", file.name, "size :",
                              os.path.getsize(os.path.join(self.config.path, file.name)))

            # Sets notification object properties
            notification = Notification(os.path.join(self.config.path, file.name))

            # persist notification in database
            notification_id = dao.persist(notification)

            # sends REST request to the loader subsystem
            request = HttpPut(self.config.loader_notification_uri)
            response = request.run({"id":notification_id})
            self.logger.debug("Put notification loader response : " + response)

    def start(self, db_connector):
        loop = asyncio.get_event_loop()
        loop.run_until_completed(self.work(db_connector))
        loop.stop()
        loop.close()
