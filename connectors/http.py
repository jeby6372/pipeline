#!/usr/bin/env python38
# -*- coding: utf-8 -*-
import json
import urllib.request
import urllib.parse



class HttpGet:
    url = None
    user = None
    password = None
    logger = None

    def __init__(self, url, user=None, password=None, logger=None):
        self.url = url
        self.user = user
        self.password = password
        self.logger = logger

    def run(self):
        if self.user is not None and self.password is not None:
            manager = urllib.request.HTTPPasswordMgrWithDefaultRealm()
            manager.add_password(None, self.url, self.user, self.password)
            auth_handler = urllib.request.HTTPBasicAuthHandler(manager)
            opener = urllib.request.build_opener(auth_handler)
            urllib.request.install_opener(opener)

        response = urllib.request.urlopen(self.url)
        res_body = response.read()

        return res_body.decode('utf-8')


class HttpPost:
    url = None

    def __init__(self, url):
        self.url = url

    # data is a dictionary of key/value to be sent
    def run(self, data):

        parms = urllib.parse.urlencode(data)
        parms = parms.encode('utf-8')

        rq_response = urllib.request.Request(self.url, data=parms, method="POST")
        response = rq_response.read()
        return response


class HttpPut:
    url = None

    def __init__(self, url):
        self.url = url

    # data is a dictionary of key/value to be sent
    def run(self, data):

        # parms = urllib.parse.urlencode(data)
        # print("HttpPut body", data)
        #parms = parms.encode('utf-8')

        urllib.request.Request(self.url, data=data, method="PUT")


class HttpRestPut:

    url = None

    def __init__(self, url):
        self.url = url

    # data is a dictionary of key/value to be sent
    def run(self):
        urllib.request.Request(self.url, data=None, method="PUT")