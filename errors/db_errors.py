#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class DatabaseConfigurationError(Exception):
    message = None

    def __init__(self, message):
        pass

    def __str__(self):
        return (repr(self.message))


class DatabaseConnectionError(Exception):
    message = None

    def __init__(self, message):
        pass

    def __str__(self):
        return (repr(self.message))


class DatabaseDuplicateError(Exception):

    def __init__(self, message):
        self.message = message