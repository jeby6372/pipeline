#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class InvalidConfigurationSyntaxError(Exception):
    message = None

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return (repr(self.message))


