#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Classe descriptive de la réception d'un fichier
"""

import os, json, datetime, time


class FileNotification:
    filename = None
    path = None
    timestamp = None
    size = None

    def __init__(self, file_path):
        self.filename = os.path.basename(file_path)
        self.path = os.path.dirname(file_path)
        self.size = os.path.getsize(file_path)
        self.timestamp = datetime.datetime.strptime(time.ctime(), "%a %b %d %H:%M:%S %Y")

    def to_json(self):
        data = {
            "host": self.host,
            "path": self.path,
            "filename": self.filename,
            "timestamp": self.timestamp.isoformat(),
            "size": self.size
        }
        return json.dumps(data)
