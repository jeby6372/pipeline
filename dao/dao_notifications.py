#!/usr/bin/env python38
# -*- coding: utf-8 -*-

import os
import psycopg2.errors as errors
from errors.db_errors import DatabaseDuplicateError as dde


class FileNotificationDao:
    __logger = None
    __connector = None
    __statements = []

    # config parameter is a model.config.DbConfig object
    def __init__(self, connector, statements, logger=None):
        self.__connector = connector
        self.__statements = statements
        self.__logger = logger

    # notification parameter is a model.notifications.FileNotification object
    def persist(self, notification):

        event_data = os.path.join(notification.path, notification.filename)

        # append notification to database

        statement = "insert into file_process (event_data, event_date) values(%s, current_timestamp) returning id"
        cur = self.__connector.cursor()

        try:
            cur.execute(statement, (event_data,))
        except errors.UniqueViolation:
            return dde(event_data + "already registered - process canceled")

        notification_id = cur.fetchone()[0]
        self.__connector.commit()
        cur.close()
        self.__connector.close()

        self.__logger.logger.info("new file notification saved " + str(notification_id))

        return notification_id


