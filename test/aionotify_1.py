#!/usr/bin/env python38
# -*- coding: utf-8 -*-
import logging
import sys,os
curr_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(curr_dir)
sys.path.append(parent_dir)
import asyncio
from connectors.logger import Log
from signal import signal, SIGINT
import aionotify
from connectors.config import RemoteConfig, DbConfig, NotifierConfig
from connectors.psql import PgsqlConnector
from dao.dao_notifications import FileNotificationDao
from model.notifications import FileNotification
from connectors.http import HttpPut, HttpRestPut
from errors.db_errors import DatabaseDuplicateError
from datetime import datetime

'''
Script de détection de présence de fichier

usage:
python38 test/aionotify_1.py -p /tmp/data
'''

# database persistency method
def persist_notification(notification):
    pass


# HTTP REST request to micre-service loader
def send_notification(notification):
    pass


# Programme entrance

if __name__ == '__main__':

    # initialization

    # application settings
    conf = RemoteConfig("/gulplug/notifier-configuration")
    data = conf.pull()
    print("Application config")
    print(data)
    app_conf = NotifierConfig(data)
    app_conf.parse()

    # set application logger
    logger = Log(app_conf.logpath, logging.DEBUG)

    # database settings
    conf = RemoteConfig("/gulplug/database-configuration")
    data = conf.pull()
    print("Database config")
    print(data)
    db_conf = DbConfig(data)
    db_conf.parse()
    db_connector = PgsqlConnector(db_conf)

    dao = FileNotificationDao(db_connector, logger)

    # optionaly creates target directory
    if not os.path.exists(app_conf.path):
        os.makedirs(app_conf.path)

    watcher = aionotify.Watcher()
    watcher.watch(alias=app_conf.path, path=app_conf.path, flags=aionotify.Flags.CLOSE_WRITE)

    loop = asyncio.get_event_loop()

    async def work():

        await watcher.setup(loop)

        while True:

            file = await watcher.get_event()

            # loads notification properties
            notification = FileNotification(os.path.join(app_conf.path, file.name))

            # persist notification in database
            result = dao.persist(notification)
            if  type(result) is DatabaseDuplicateError:
                print(result.message)
                continue
            '''
            print("new file detected :", file.name, "size :",
                         os.path.getsize(os.path.join(app_conf.path, file.name)), "ID", str(result))
            '''

            """
            logger.logger.info("new file detected : " + file.name, "size : " +
                         str(os.path.getsize(os.path.join(app_conf.path, file.name))) +  " ID " + str(id))
            """

            # sends REST request to loader sub system

            url = "http://" + app_conf.loader_host + ":" + str(app_conf.loader_port) + app_conf.loader_notification_uri + "/" + str(result)
            logger.logger.info("Notification sent to " + url)
            #print("Notification REST request " + url)

            data = {
                "Id":result,
                "EventData":os.path.join(notification.path, notification.filename),
                "EventDate":datetime.now().isoformat()
            }

            request = HttpPut(url)
            request.run(data)



    def handler(signal_received, frame):
        print('Scanner stop requested')
        exit(0)


    signal(SIGINT, handler)

    try:
        print("File detection test started on", app_conf.path)
        loop.run_until_complete(work())
    finally:
        loop.stop()
        loop.close()
        watcher.unwatch(app_conf.path)
