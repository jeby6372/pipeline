#!/usr/bin/env python38
# -*- coding: utf-8 -*-
import argparse
import os, sys
curr_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(curr_dir)
sys.path.append(parent_dir)

from connectors.logger import Log
from connectors.config import RemoteConfig

'''
Extract configuration from a specific context

usage :
python38 test/congig.py /gulplug/database-configuration
'''

if __name__ == '__main__':

    '''
    context is the resource url without extension eg : /gulplug/database-configuration
    '''
    parser = argparse.ArgumentParser()
    parser.add_argument('context', help="Configuration file url")
    args = parser.parse_args()

    print('-> Starting Config test')
    print('context :', args.context)

    l = Log('test-config.log')

    host = os.environ['CONFIGURATION_HOST']
    port = os.environ['CONFIGURATION_PORT']
    user = os.environ['CONFIGURATION_USER']
    password = os.environ['CONFIGURATION_PASSWORD']

    l.logger.info("configuration server host : %s", host)
    l.logger.info("configuration server port : %s", port)
    l.logger.info("configuration server user : %s", user)
    l.logger.info("configuration server password : %s", password)

    conf = RemoteConfig(args.context, l.logger)
    data = conf.pull()
    print(data)

    print("-> End of Config test")
