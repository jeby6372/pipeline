#!/usr/bin/env python38
# -*- coding: utf-8 -*-
import argparse
import os, sys
from datetime import datetime

curr_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(curr_dir)
sys.path.append(parent_dir)
import logging
from connectors.config import RemoteConfig, DbConfig
from connectors.psql import PgsqlConnector

'''
Check the database access using a specific configuration context

scripts/env.sh should be adapted and run to target the right configuration server before running this script
'''

TABLE = "operators"


def getConfig():
    return os.environ['CONFIGURATION_HOST'], \
           os.environ['CONFIGURATION_PORT'], \
           os.environ['CONFIGURATION_USER'], \
           os.environ['CONFIGURATION_PASSWORD']


if __name__ == '__main__':
    '''
    context is the configuration resource url eg : /pipeline/database-configuration.json
    '''
    parser = argparse.ArgumentParser()
    parser.add_argument('context', help="Configuration file url")
    args = parser.parse_args()

print('-> Starting Database Access test')

logger = logging.getLogger('test-database-access')

host, port, user, password = getConfig()

print("configuration server host :", host)
print("configuration server port :", port)
print("configuration server user :", user)
print("configuration server host :", password)

conf = RemoteConfig(args.context)
data = conf.pull()

print('context :', args.context)
print(data)

db_conf = DbConfig(data)
db_conf.parse()
db_connector = PgsqlConnector(db_conf)
connection = db_connector.connect()

print("connection", connection)
print("cursor", connection.cursor())

statement = "insert into operators (username, password, email, created_on) values(%s, %s,%s, current_timestamp)"
connection.cursor().execute(statement, ('quiminfo', 'quiminfo', 'contact@quiminfo.com'))
connection.commit()

db_connector.close()
print("connection", db_connector.connection)

print("-> End of Database-Access test")
