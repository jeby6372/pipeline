#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import os
import random
import string

'''
Génère des fichiers texte dans le répertoire indiqué en paramètre

usage:
aionotify/test_1.py -p /tmp/data -l 1000 -s 1000 
'''

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--path', default="/tmp", help="Path target")
    parser.add_argument('-c', '--count', type=int, default=10, help="Number of files to create")
    parser.add_argument('-l', '--lines', type=int, default=100, help="Number of lines inside each file")
    parser.add_argument('-s', '--size', type=int, default=30, help="line character length")
    args = parser.parse_args()

    # create directory target
    if not os.path.exists(args.path):
        os.makedirs(args.path)



    # create sample files
    for i in range(args.count):

        path = os.path.join(args.path, "sample-" + str(i) + ".tar.gz")

        fp = open(path, "w")
        for l in range(args.lines):
            line = ''.join(random.choice(string.ascii_uppercase + string.digits + string.ascii_lowercase) for _ in range(args.size))
            fp.write(line + "\n")

        fp.close()
        print("Appending file", path, "lines", args.lines, "length", args.size)

    print("Test completed for", args.count, "files")