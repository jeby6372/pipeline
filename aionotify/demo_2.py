#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import asyncio
import logging
import os
from signal import signal, SIGINT
import aionotify

'''
Script de détection de présence de fichier

usage:
aionotify/demo_2.py -p /tmp/data
'''


# database persistency method
def persist_notification(notification):
    pass


# HTTP REST request to micre-service loader
def send_notification(notification):
    pass


# Programme entrance

if __name__ == '__main__':

    # get input arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--path', default="/tmp", help="Path to listen to")
    args = parser.parse_args()

    # initialization
    logger = logging.getLogger('notifier')
    logger.debug("initialization in progress")

    watcher = aionotify.Watcher()

    watcher.watch(alias=args.path, path=args.path, flags=aionotify.Flags.CLOSE_WRITE)

    loop = asyncio.get_event_loop()

    async def work():

        await watcher.setup(loop)
        while True:
            file = await watcher.get_event()
            logger.debug("new file detected :", file.name, "size :",
                         os.path.getsize(os.path.join(args.path, file.name)))

            # loads notification properties

            # persist notification in database


    def handler(signal_received, frame):
        print('Scanner stop requested')
        exit(0)


    signal(SIGINT, handler)

    try:
        logger.info("Starting notifier service")
        loop.run_until_complete(work())
    finally:
        loop.stop()
        loop.close()
        watcher.unwatch(args.path)
