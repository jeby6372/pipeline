#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys

curr_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(curr_dir)
sys.path.append(parent_dir)
# import argparse
from connectors.fs_scanner import Scanner
from connectors.psql import PgsqlConnector
from connectors.config import RemoteConfig, DbConfig, NotifierConfig
from connectors.logger import Log

'''
Starts the file system scanner

scripts/env-*.sh should be adapted and launch to target the right configuration before running this script
'''

if __name__ == '__main__':

    """
    CONFIGURATION_HOST: host address
    CONFIGURATION_PORT: host port
    CONFIGURATION_USER: user
    CONFIGURATION_PASSWORD: password
    """


    # fetch database configuration and provides a DB connector
    remote_conf = RemoteConfig("/gulplug/database-configuration")
    data = remote_conf.pull()
    db_conf = DbConfig(data)
    db_conf.parse()
    db_connector = PgsqlConnector(db_conf)

    # fetch application configuration
    remote_conf = RemoteConfig("/gulplug/notifier-configuration")
    data = remote_conf.pull()
    app_conf = NotifierConfig(data)
    app_conf.parse()

    # builds application logger
    logger = Log(app_conf.logpath)

    # starts the file system scanner
    watcher = Scanner(app_conf, logger)
    watcher.start(db_connector)
