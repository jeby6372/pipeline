#!/bin/bash

# usage:
# source scripts/env-clear.sh

export CONFIGURATION_HOST=
export CONFIGURATION_PORT=
export CONFIGURATION_USER=
export CONFIGURATION_PASSWORD=

echo "Environment variables cleared"
